# explicit wildcard expansion suppresses errors when no files are found
include $(wildcard *.deps)

SOURCES=$(shell find . -name '*.scad')
MODELS=$(SOURCES:%.scad=%.stl)
IMAGES=$(SOURCES:%.scad=%.png)

all: $(MODELS) $(IMAGES)
models: $(MODELS)
images: $(IMAGES)

%.stl: %.scad
	openscad -m make -o $@ -d $@.deps $<

%.png: %.scad
	openscad -m make -o $@ -d $@.deps --viewall --autocenter $<

view: $(IMAGES)
	open $(IMAGES)
