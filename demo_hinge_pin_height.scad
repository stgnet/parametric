use <standard_hinge.scad>

for (i=[0:6]) {
	translate([i*50, 0, 0]) standard_hinge(pin_center_height=2*i-2);
}

/* vim: set ts=4 sw=4 sts=4 noet : */
