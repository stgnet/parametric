$fn=64;

/* parametric standard hinge
        * open position with no swag (unless configured)
        * printed in place
        * non-removable captured pin
        * pin is along y axis
        * reinforced knuckles extruded from leaf for improved strength
        * all values can be modified and have reasonable defaults

    PARAMETERS:

    knuckles            = number of knuckles, defaults to 7
                            * if even pin will be exposed and unsupported on one end
                            * if  set to 2, the two leafs can be separated (flag hinge)

    hinge_length        = y dimension of entire hinge starting at y=0
                            * defaults to 8mm per kuckle

    hinge_width            = x dimension of entire hinge with center on x=0
                            * defaults to 40mm

    knuckle_diameter    = diameter of knuckles around pin
                            * defaults to 1/5 of hinge_width

    pin_center_height    = distance from bottom of hinge to center of pin
                            * defaults to half of knuckle_diameter
                            * if less than half of knuckle_diameter causes swag

    pin_diameter        = diameter of pin inside knuckles
                            * defaults to one half of knuckle diameter

    leaf_height            = thickness (z dimension) of the leafs
                            * defaults to one quarter knuckle diameter

    base_width            = distance between center pin and knuckle supports
                            * defaults to knuckle diameter (~45 angle on knuckle support)
                            * reducing this from default weakens hinge strength

    angle               = degrees of tilt added to sides of knuckles
                            * defaults to 0, positive or negative value selects tilt side
                            * for building knuckles around pin that is on Z axis

    angle_end           = tilt applied to first/last knuckle
                            * defaults to true
                            * set to false if the knuckle is on print bed

    pin                 = include the pin
                            * defaults to true, left (-x) leaf knuckles attached to pin
                            * if false, left knuckles have hole for pin matching right knuckles

    left                = include the left leaf (-x) and knuckles
                            * defaults to true
                            * set to false to print only the right leaf

    right               = include the right leaf (-x) and knuckles
                            * defaults to true
                            * set to false to print only the left leaf and pin (if also true)

    gap                    = distance in mm between all moving parts
                            * defaults to 0.5
                             * .4 or less may cause hinge parts to fuse together

*/

module standard_hinge(hinge_length, hinge_width, knuckles,
                        knuckle_diameter, pin_center_height, pin_diameter,
                        holes, bevel, hole_diameter, hole_offset,
                        leaf_height, left_height, right_height, left_swag, right_swag,
                        pin, left, right, angle, angle_end, base_width, gap, debug) {

    // set defaults
    knuckles=knuckles?knuckles:7;
    hinge_width=hinge_width?hinge_width:40;
    hinge_length=hinge_length?hinge_length:8*knuckles;
    knuckle_diameter=knuckle_diameter?knuckle_diameter:0.2*hinge_width;
    gap=gap?gap:0.5;
    leaf_height=leaf_height?leaf_height:knuckle_diameter/4;
    pin_center_height=pin_center_height!=undef?pin_center_height:knuckle_diameter/2;
    pin_diameter=pin_diameter?pin_diameter:knuckle_diameter/2;
    base_width=base_width?base_width:knuckle_diameter;
    left_height=left_height?left_height:leaf_height;
    right_height=right_height?right_height:leaf_height;
    left_height_with_swag=left_height+left_swag;
    right_height_with_swag=right_height+right_swag;
    pin=pin!=undef?pin:true;
    left=left!=undef?left:true;
    right=right!=undef?right:true;
    angle=angle?angle:0;
    angle_end=angle_end!=undef?angle_end:true;

    // convenience calculations
    knuckle_pitch=(hinge_length+gap)/knuckles;
    knuckle_length=knuckle_pitch-gap;
    leaf_width=hinge_width/2;

    // set width of knuckle base supports to clear the pin
    left_clearance_z=pin_center_height-(left_height_with_swag<pin_center_height?left_height_with_swag:pin_center_height);
    left_clearance_a=knuckle_diameter/2+gap;
    left_clearance_x_calculated=sqrt(pow(left_clearance_a,2)-pow(left_clearance_z,2));
    left_clearance_x=left_clearance_x_calculated==left_clearance_x_calculated?left_clearance_x_calculated:gap/2;
    left_clearance_width=base_width-left_clearance_x;
    left_flat_width=leaf_width-left_clearance_x;

    right_clearance_z=pin_center_height-(right_height_with_swag<pin_center_height?right_height_with_swag:pin_center_height);
    right_clearance_a=knuckle_diameter/2+gap;
    right_clearance_x_calculated=sqrt(pow(right_clearance_a,2)-pow(right_clearance_z,2));
    right_clearance_x=right_clearance_x_calculated==right_clearance_x_calculated?right_clearance_x_calculated:gap/2;
    right_clearance_width=base_width-right_clearance_x;
    right_flat_width=leaf_width-right_clearance_x;

    // additional defaults
    hole_diameter=hole_diameter?hole_diameter:pin_diameter;
    hole_diameter2=bevel?hole_diameter+leaf_height:hole_diameter;

    left_leaf_center=(leaf_width-base_width)/2;
    left_hole_offset=hole_offset?hole_offset:left_flat_width/8;

    right_leaf_center=(leaf_width-base_width)/2;
    right_hole_offset=hole_offset?hole_offset:right_flat_width/8;

    if (debug) {
        echo("knuckles", knuckles);
        echo("hinge_width", hinge_width);
        echo("hinge_lenth", hinge_length);
        echo("kuckle_diameter", knuckle_diameter);
        echo("leaf_height", leaf_height);
        echo("pin_center_height", pin_center_height);
        echo("pin_diameter", pin_diameter);
        echo("hole_diameter", hole_diameter);
        echo("base_width", base_width);
        echo("gap", gap);
        echo("knuckle_pitch", knuckle_pitch);
        echo("knuckle_length", knuckle_length);
        echo("leaf_width", leaf_width);
    }

    union() {
        if (left) {
            // pin side (-x) left leaf
            translate([-leaf_width, 0, left_swag]) {
                difference() {
                    cube([left_flat_width, hinge_length, left_height]);
                    if (holes) {
                        for (i=[2:2:knuckles]) {
                            translate([left_leaf_center+(i%4?1:-1)*left_hole_offset, i*knuckle_pitch-knuckle_pitch/2, 0])
                                cylinder(d1=hole_diameter, d2=hole_diameter2, h=left_height);
                        }
                    }
                }
            }
        }

        if (right) {
            // non-pin side (+x) right leaf
            translate([right_clearance_x, 0, right_swag]) {
                difference() {
                    cube([right_flat_width, hinge_length, right_height]);
                    if (holes) {
                        for (i=[2:2:knuckles]) {
                            translate([right_flat_width-(right_leaf_center+(i%4?1:-1)*right_hole_offset), i*knuckle_pitch-knuckle_pitch/2, 0])
                                cylinder(d1=hole_diameter, d2=hole_diameter2, h=right_height);
                        }
                    }
                }
            }
        }

        // pin
        if (pin) {
            rotate([-90, 0, 0])
                translate([0, -pin_center_height, 0])
                    cylinder(d=pin_diameter-gap, h=hinge_length);
        }

        // knuckles
        for (i=[1:knuckles]) {
            if (i%2) {
                if (left) {
                    // knuckle with pin - left leaf
                    translate([0, i*knuckle_pitch-gap, 0]) rotate([90, 0, 0])
                    difference() {
                        hull() {
                            translate([0, pin_center_height, 0]) {
                                cylinder(d=knuckle_diameter, h=knuckle_length);
                            }
                            translate([-base_width, left_swag, 0]) {
                                cube([left_clearance_width/2, left_height, knuckle_length]);
                            }
                        }
                        if (!pin) {
                            translate([0, pin_center_height, 0]) {
                                cylinder(d=pin_diameter+gap, h=knuckle_length);
                            }
                        }
                        if ((angle>0 ? i!=1 : i!=knuckles) || angle_end) {
                            translate([-base_width, 0, angle>0?knuckle_length:-knuckle_length])
                                rotate([0, angle, 0])
                                    cube([base_width+knuckle_diameter,knuckle_diameter,knuckle_pitch]);
                        }
                    }
                }
            } else {
                if (right) {
                    // knuckle without pin - right leaf
                    translate([0, i*knuckle_pitch-gap, 0]) rotate([90, 0, 0])
                    difference() {
                        hull() {
                            translate([0, pin_center_height, 0]) {
                                cylinder(d=knuckle_diameter, h=knuckle_length);
                            }
                            translate([(base_width)-right_clearance_width, right_swag, 0]) {
                                cube([right_clearance_width, right_height, knuckle_length]);
                            }
                        }
                        translate([0, pin_center_height, 0]) {
                            cylinder(d=pin_diameter+gap, h=knuckle_length);
                        }
                        if ((angle>0 ? i!=1 : i!=knuckles) || angle_end) {
                            translate([-base_width, 0, angle>0?knuckle_length:-knuckle_length])
                                rotate([0, angle, 0])
                                    cube([base_width+knuckle_diameter,knuckle_diameter,knuckle_pitch]);
                        }
                    }
                }
            }
        }
    }
}

standard_hinge();

translate([50, 0, 0]) rotate([90, 0, 0]) standard_hinge(holes=true, bevel=true, right_swag=5, pin=true, angle=5, angle_end=false);

/* vim: set ts=4 sw=4 sts=4 et : */
