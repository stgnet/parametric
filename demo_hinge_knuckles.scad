use <standard_hinge.scad>

for (i=[0:6]) {
	translate([i*50, 0, 0]) standard_hinge(knuckles=i+2);
}

/* vim: set ts=4 sw=4 sts=4 noet : */
