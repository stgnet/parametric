use <standard_hinge.scad>

for (i=[0:5]) {
	translate([i*50, 0, 0]) standard_hinge(leaf_height=i*2+1);
}

/* vim: set ts=4 sw=4 sts=4 noet : */
